using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;

using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml;  

namespace WindowsFormsApplication1
{
    public partial class Form2 : Form
    {
        string g;

        public Form2(string x)
        {
            InitializeComponent();
            this.g = x;
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string user,pass;
            string content;
            user = textBox1.Text;
            pass = textBox2.Text;
            HttpWebRequest request = WebRequest.Create("http://localhost:8080/Contazul-war/webresources/blue.usuarios") as HttpWebRequest;

            // Get response  
            
            
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                // Get the response stream  
                StreamReader reader = new StreamReader(response.GetResponseStream());

                // Console application output  
                //Console.WriteLine(reader.ReadToEnd());

                content = reader.ReadToEnd();

                // Console application output  
                
            
            
            }
            
            //xmlDoc.LoadXml(content);
            XmlReader xmlReader = XmlReader.Create(new StringReader(content));


            var document = XElement.Load(xmlReader);
            var persons = document.Elements("usuarios")
            .Select(p => new Person { Name = p.Element("pass").Value });



            //string todo = "ivan";

            foreach (Person p in persons)
            {
                Console.WriteLine(p.Name);
                if (pass.Equals(p.Name))
                { 
                    
                    Form1 f1 = new Form1(this.g);
                    this.Hide();

                    f1.Show();
                    f1.Focus();
                }
            }

        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        public class Person
        {
            public string Name { get; set; }
        } 
    }
}

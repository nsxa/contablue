package com.example.ivan.httprequest;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;


public class MainActivity extends ActionBarActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        LongOperation tarea = new LongOperation();
        tarea.onPreExecute();

        //tarea.doInBackground(null);

    }

    private class LongOperation extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] params) {
            return null;
        }

        @Override
        protected void onPreExecute() {
            String postUrl="http://ip:8080/xxx-war/webresources/";// put in your url

            HttpPost post = new HttpPost(postUrl);
            StringEntity postingString = null;
            try {
                postingString = new StringEntity("<companies>\n" +
                        "<idCompanies>11</idCompanies>\n" +
                        "<nit>151515</nit>\n" +
                        "<nombre>GQ111111</nombre>\n" +
                        "</companies>");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            post.setEntity(postingString);
            post.setHeader("Content-type","application/xml");
            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse resp;
            try {
                resp=httpClient.execute(post);

                HttpEntity entity = resp.getEntity();
                String responseString = EntityUtils.toString(entity,"UTF-8");
                System.out.println(responseString);

            } catch (IOException e) {
                e.printStackTrace();

            }

        }


        protected void onPostExecute(String result) {
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

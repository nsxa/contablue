private void InitializeComponent(string p)
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(48, 55);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(421, 331);
            this.dataGridView1.TabIndex = 0;

            
            DataSet ds = new DataSet();
            ds.ReadXml(XmlReader.Create(new StringReader(p)));

            this.dataGridView1.DataSource = ds.Tables[0];

            //dataSet.ReadXml();
            // 
            // Form1
            // 
            
            this.ClientSize = new System.Drawing.Size(565, 488);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

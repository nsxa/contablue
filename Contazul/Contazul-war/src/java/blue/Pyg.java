/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blue;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ivan
 */
@Entity
@Table(name = "pyg")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pyg.findAll", query = "SELECT p FROM Pyg p"),
    @NamedQuery(name = "Pyg.findById", query = "SELECT p FROM Pyg p WHERE p.id = :id")})
public class Pyg implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pYGid")
    private Collection<PygHasCuentasHasCompanies> pygHasCuentasHasCompaniesCollection;
    @JoinColumn(name = "Usuarios_idUsuarios", referencedColumnName = "idUsuarios")
    @ManyToOne(optional = false)
    private Usuarios usuariosidUsuarios;
    @JoinColumn(name = "Companies_idCompanies", referencedColumnName = "idCompanies")
    @ManyToOne(optional = false)
    private Companies companiesidCompanies;

    public Pyg() {
    }

    public Pyg(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @XmlTransient
    public Collection<PygHasCuentasHasCompanies> getPygHasCuentasHasCompaniesCollection() {
        return pygHasCuentasHasCompaniesCollection;
    }

    public void setPygHasCuentasHasCompaniesCollection(Collection<PygHasCuentasHasCompanies> pygHasCuentasHasCompaniesCollection) {
        this.pygHasCuentasHasCompaniesCollection = pygHasCuentasHasCompaniesCollection;
    }

    public Usuarios getUsuariosidUsuarios() {
        return usuariosidUsuarios;
    }

    public void setUsuariosidUsuarios(Usuarios usuariosidUsuarios) {
        this.usuariosidUsuarios = usuariosidUsuarios;
    }

    public Companies getCompaniesidCompanies() {
        return companiesidCompanies;
    }

    public void setCompaniesidCompanies(Companies companiesidCompanies) {
        this.companiesidCompanies = companiesidCompanies;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pyg)) {
            return false;
        }
        Pyg other = (Pyg) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "blue.Pyg[ id=" + id + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blue;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ivan
 */
@Entity
@Table(name = "companies")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Companies.findAll", query = "SELECT c FROM Companies c"),
    @NamedQuery(name = "Companies.findByIdCompanies", query = "SELECT c FROM Companies c WHERE c.idCompanies = :idCompanies"),
    @NamedQuery(name = "Companies.findByNombre", query = "SELECT c FROM Companies c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "Companies.findByNit", query = "SELECT c FROM Companies c WHERE c.nit = :nit")})
public class Companies implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idCompanies")
    private Integer idCompanies;
    @Size(max = 45)
    @Column(name = "Nombre")
    private String nombre;
    @Size(max = 45)
    @Column(name = "Nit")
    private String nit;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "companiesidCompanies1")
    private Collection<Balance> balanceCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "companiesidCompanies")
    private Collection<Pyg> pygCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "companiesidCompanies1")
    private Collection<CuentasHasCompanies> cuentasHasCompaniesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "companiesidCompanies")
    private Collection<Usuarios> usuariosCollection;

    public Companies() {
    }

    public Companies(Integer idCompanies) {
        this.idCompanies = idCompanies;
    }

    public Integer getIdCompanies() {
        return idCompanies;
    }

    public void setIdCompanies(Integer idCompanies) {
        this.idCompanies = idCompanies;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    @XmlTransient
    public Collection<Balance> getBalanceCollection() {
        return balanceCollection;
    }

    public void setBalanceCollection(Collection<Balance> balanceCollection) {
        this.balanceCollection = balanceCollection;
    }

    @XmlTransient
    public Collection<Pyg> getPygCollection() {
        return pygCollection;
    }

    public void setPygCollection(Collection<Pyg> pygCollection) {
        this.pygCollection = pygCollection;
    }

    @XmlTransient
    public Collection<CuentasHasCompanies> getCuentasHasCompaniesCollection() {
        return cuentasHasCompaniesCollection;
    }

    public void setCuentasHasCompaniesCollection(Collection<CuentasHasCompanies> cuentasHasCompaniesCollection) {
        this.cuentasHasCompaniesCollection = cuentasHasCompaniesCollection;
    }

    @XmlTransient
    public Collection<Usuarios> getUsuariosCollection() {
        return usuariosCollection;
    }

    public void setUsuariosCollection(Collection<Usuarios> usuariosCollection) {
        this.usuariosCollection = usuariosCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCompanies != null ? idCompanies.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Companies)) {
            return false;
        }
        Companies other = (Companies) object;
        if ((this.idCompanies == null && other.idCompanies != null) || (this.idCompanies != null && !this.idCompanies.equals(other.idCompanies))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "blue.Companies[ idCompanies=" + idCompanies + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blue;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ivan
 */
@Entity
@Table(name = "cuent_has_comp_has_bal")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CuentHasCompHasBal.findAll", query = "SELECT c FROM CuentHasCompHasBal c"),
    @NamedQuery(name = "CuentHasCompHasBal.findById", query = "SELECT c FROM CuentHasCompHasBal c WHERE c.id = :id")})
public class CuentHasCompHasBal implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "Cuentas_has_Companies_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private CuentasHasCompanies cuentashasCompaniesid;
    @JoinColumn(name = "Balance_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Balance balanceid;

    public CuentHasCompHasBal() {
    }

    public CuentHasCompHasBal(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CuentasHasCompanies getCuentashasCompaniesid() {
        return cuentashasCompaniesid;
    }

    public void setCuentashasCompaniesid(CuentasHasCompanies cuentashasCompaniesid) {
        this.cuentashasCompaniesid = cuentashasCompaniesid;
    }

    public Balance getBalanceid() {
        return balanceid;
    }

    public void setBalanceid(Balance balanceid) {
        this.balanceid = balanceid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CuentHasCompHasBal)) {
            return false;
        }
        CuentHasCompHasBal other = (CuentHasCompHasBal) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "blue.CuentHasCompHasBal[ id=" + id + " ]";
    }
    
}

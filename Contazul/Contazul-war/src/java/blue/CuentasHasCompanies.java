/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blue;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ivan
 */
@Entity
@Table(name = "cuentas_has_companies")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CuentasHasCompanies.findAll", query = "SELECT c FROM CuentasHasCompanies c"),
    @NamedQuery(name = "CuentasHasCompanies.findById", query = "SELECT c FROM CuentasHasCompanies c WHERE c.id = :id")})
public class CuentasHasCompanies implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cuentashasCompaniesid")
    private Collection<PygHasCuentasHasCompanies> pygHasCuentasHasCompaniesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cuentashasCompaniesid")
    private Collection<CuentHasCompHasBal> cuentHasCompHasBalCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cuentashasCompaniesid")
    private Collection<RegistrosHasCompanies> registrosHasCompaniesCollection;
    @JoinColumn(name = "Cuentas_idCuentas1", referencedColumnName = "idCuentas")
    @ManyToOne(optional = false)
    private Cuentas cuentasidCuentas1;
    @JoinColumn(name = "Companies_idCompanies1", referencedColumnName = "idCompanies")
    @ManyToOne(optional = false)
    private Companies companiesidCompanies1;

    public CuentasHasCompanies() {
    }

    public CuentasHasCompanies(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @XmlTransient
    public Collection<PygHasCuentasHasCompanies> getPygHasCuentasHasCompaniesCollection() {
        return pygHasCuentasHasCompaniesCollection;
    }

    public void setPygHasCuentasHasCompaniesCollection(Collection<PygHasCuentasHasCompanies> pygHasCuentasHasCompaniesCollection) {
        this.pygHasCuentasHasCompaniesCollection = pygHasCuentasHasCompaniesCollection;
    }

    @XmlTransient
    public Collection<CuentHasCompHasBal> getCuentHasCompHasBalCollection() {
        return cuentHasCompHasBalCollection;
    }

    public void setCuentHasCompHasBalCollection(Collection<CuentHasCompHasBal> cuentHasCompHasBalCollection) {
        this.cuentHasCompHasBalCollection = cuentHasCompHasBalCollection;
    }

    @XmlTransient
    public Collection<RegistrosHasCompanies> getRegistrosHasCompaniesCollection() {
        return registrosHasCompaniesCollection;
    }

    public void setRegistrosHasCompaniesCollection(Collection<RegistrosHasCompanies> registrosHasCompaniesCollection) {
        this.registrosHasCompaniesCollection = registrosHasCompaniesCollection;
    }

    public Cuentas getCuentasidCuentas1() {
        return cuentasidCuentas1;
    }

    public void setCuentasidCuentas1(Cuentas cuentasidCuentas1) {
        this.cuentasidCuentas1 = cuentasidCuentas1;
    }

    public Companies getCompaniesidCompanies1() {
        return companiesidCompanies1;
    }

    public void setCompaniesidCompanies1(Companies companiesidCompanies1) {
        this.companiesidCompanies1 = companiesidCompanies1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CuentasHasCompanies)) {
            return false;
        }
        CuentasHasCompanies other = (CuentasHasCompanies) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "blue.CuentasHasCompanies[ id=" + id + " ]";
    }
    
}

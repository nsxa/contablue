/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blue;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ivan
 */
@Entity
@Table(name = "registros")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Registros.findAll", query = "SELECT r FROM Registros r"),
    @NamedQuery(name = "Registros.findByIdRegistros", query = "SELECT r FROM Registros r WHERE r.idRegistros = :idRegistros"),
    @NamedQuery(name = "Registros.findByRegistro", query = "SELECT r FROM Registros r WHERE r.registro = :registro"),
    @NamedQuery(name = "Registros.findByFecha", query = "SELECT r FROM Registros r WHERE r.fecha = :fecha")})
public class Registros implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idRegistros")
    private Integer idRegistros;
    @Size(max = 45)
    @Column(name = "Registro")
    private String registro;
    @Column(name = "Fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "registrosidRegistros")
    private Collection<RegistrosHasCompanies> registrosHasCompaniesCollection;

    public Registros() {
    }

    public Registros(Integer idRegistros) {
        this.idRegistros = idRegistros;
    }

    public Integer getIdRegistros() {
        return idRegistros;
    }

    public void setIdRegistros(Integer idRegistros) {
        this.idRegistros = idRegistros;
    }

    public String getRegistro() {
        return registro;
    }

    public void setRegistro(String registro) {
        this.registro = registro;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @XmlTransient
    public Collection<RegistrosHasCompanies> getRegistrosHasCompaniesCollection() {
        return registrosHasCompaniesCollection;
    }

    public void setRegistrosHasCompaniesCollection(Collection<RegistrosHasCompanies> registrosHasCompaniesCollection) {
        this.registrosHasCompaniesCollection = registrosHasCompaniesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegistros != null ? idRegistros.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Registros)) {
            return false;
        }
        Registros other = (Registros) object;
        if ((this.idRegistros == null && other.idRegistros != null) || (this.idRegistros != null && !this.idRegistros.equals(other.idRegistros))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "blue.Registros[ idRegistros=" + idRegistros + " ]";
    }
    
}

package blue;

import blue.Balance;
import blue.Companies;
import blue.Pyg;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-03-30T10:12:07")
@StaticMetamodel(Usuarios.class)
public class Usuarios_ { 

    public static volatile CollectionAttribute<Usuarios, Pyg> pygCollection;
    public static volatile CollectionAttribute<Usuarios, Balance> balanceCollection;
    public static volatile SingularAttribute<Usuarios, Integer> idUsuarios;
    public static volatile SingularAttribute<Usuarios, Companies> companiesidCompanies;
    public static volatile SingularAttribute<Usuarios, String> user;
    public static volatile SingularAttribute<Usuarios, String> pass;

}
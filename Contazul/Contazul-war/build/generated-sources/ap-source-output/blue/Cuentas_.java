package blue;

import blue.CuentasHasCompanies;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-03-30T10:12:07")
@StaticMetamodel(Cuentas.class)
public class Cuentas_ { 

    public static volatile SingularAttribute<Cuentas, String> nombre;
    public static volatile SingularAttribute<Cuentas, Integer> tipo;
    public static volatile SingularAttribute<Cuentas, Integer> idCuentas;
    public static volatile SingularAttribute<Cuentas, Integer> pasivo;
    public static volatile SingularAttribute<Cuentas, Integer> activo;
    public static volatile CollectionAttribute<Cuentas, CuentasHasCompanies> cuentasHasCompaniesCollection;

}
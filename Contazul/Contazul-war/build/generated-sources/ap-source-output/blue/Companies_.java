package blue;

import blue.Balance;
import blue.CuentasHasCompanies;
import blue.Pyg;
import blue.Usuarios;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-03-30T10:12:07")
@StaticMetamodel(Companies.class)
public class Companies_ { 

    public static volatile SingularAttribute<Companies, String> nombre;
    public static volatile CollectionAttribute<Companies, Pyg> pygCollection;
    public static volatile SingularAttribute<Companies, String> nit;
    public static volatile CollectionAttribute<Companies, Balance> balanceCollection;
    public static volatile CollectionAttribute<Companies, Usuarios> usuariosCollection;
    public static volatile SingularAttribute<Companies, Integer> idCompanies;
    public static volatile CollectionAttribute<Companies, CuentasHasCompanies> cuentasHasCompaniesCollection;

}
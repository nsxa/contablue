package blue;

import blue.RegistrosHasCompanies;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-03-30T10:12:07")
@StaticMetamodel(Registros.class)
public class Registros_ { 

    public static volatile SingularAttribute<Registros, Date> fecha;
    public static volatile SingularAttribute<Registros, String> registro;
    public static volatile SingularAttribute<Registros, Integer> idRegistros;
    public static volatile CollectionAttribute<Registros, RegistrosHasCompanies> registrosHasCompaniesCollection;

}
package blue;

import blue.Companies;
import blue.CuentHasCompHasBal;
import blue.Cuentas;
import blue.PygHasCuentasHasCompanies;
import blue.RegistrosHasCompanies;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-03-30T10:12:07")
@StaticMetamodel(CuentasHasCompanies.class)
public class CuentasHasCompanies_ { 

    public static volatile SingularAttribute<CuentasHasCompanies, Integer> id;
    public static volatile CollectionAttribute<CuentasHasCompanies, CuentHasCompHasBal> cuentHasCompHasBalCollection;
    public static volatile CollectionAttribute<CuentasHasCompanies, RegistrosHasCompanies> registrosHasCompaniesCollection;
    public static volatile CollectionAttribute<CuentasHasCompanies, PygHasCuentasHasCompanies> pygHasCuentasHasCompaniesCollection;
    public static volatile SingularAttribute<CuentasHasCompanies, Cuentas> cuentasidCuentas1;
    public static volatile SingularAttribute<CuentasHasCompanies, Companies> companiesidCompanies1;

}
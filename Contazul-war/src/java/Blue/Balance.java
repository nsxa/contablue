/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Blue;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ivan
 */
@Entity
@Table(name = "balance")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Balance.findAll", query = "SELECT b FROM Balance b"),
    @NamedQuery(name = "Balance.findById", query = "SELECT b FROM Balance b WHERE b.id = :id")})
public class Balance implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "Usuarios_idUsuarios1", referencedColumnName = "idUsuarios")
    @ManyToOne(optional = false)
    private Usuarios usuariosidUsuarios1;
    @JoinColumn(name = "Companies_idCompanies1", referencedColumnName = "idCompanies")
    @ManyToOne(optional = false)
    private Companies companiesidCompanies1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "balanceid")
    private Collection<CuentHasCompHasBal> cuentHasCompHasBalCollection;

    public Balance() {
    }

    public Balance(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Usuarios getUsuariosidUsuarios1() {
        return usuariosidUsuarios1;
    }

    public void setUsuariosidUsuarios1(Usuarios usuariosidUsuarios1) {
        this.usuariosidUsuarios1 = usuariosidUsuarios1;
    }

    public Companies getCompaniesidCompanies1() {
        return companiesidCompanies1;
    }

    public void setCompaniesidCompanies1(Companies companiesidCompanies1) {
        this.companiesidCompanies1 = companiesidCompanies1;
    }

    @XmlTransient
    public Collection<CuentHasCompHasBal> getCuentHasCompHasBalCollection() {
        return cuentHasCompHasBalCollection;
    }

    public void setCuentHasCompHasBalCollection(Collection<CuentHasCompHasBal> cuentHasCompHasBalCollection) {
        this.cuentHasCompHasBalCollection = cuentHasCompHasBalCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Balance)) {
            return false;
        }
        Balance other = (Balance) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Blue.Balance[ id=" + id + " ]";
    }
    
}

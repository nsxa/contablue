/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Blue;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ivan
 */
@Entity
@Table(name = "pyg_has_cuentas_has_companies")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PygHasCuentasHasCompanies.findAll", query = "SELECT p FROM PygHasCuentasHasCompanies p"),
    @NamedQuery(name = "PygHasCuentasHasCompanies.findById", query = "SELECT p FROM PygHasCuentasHasCompanies p WHERE p.id = :id")})
public class PygHasCuentasHasCompanies implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "Cuentas_has_Companies_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private CuentasHasCompanies cuentashasCompaniesid;
    @JoinColumn(name = "PYG_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Pyg pYGid;

    public PygHasCuentasHasCompanies() {
    }

    public PygHasCuentasHasCompanies(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CuentasHasCompanies getCuentashasCompaniesid() {
        return cuentashasCompaniesid;
    }

    public void setCuentashasCompaniesid(CuentasHasCompanies cuentashasCompaniesid) {
        this.cuentashasCompaniesid = cuentashasCompaniesid;
    }

    public Pyg getPYGid() {
        return pYGid;
    }

    public void setPYGid(Pyg pYGid) {
        this.pYGid = pYGid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PygHasCuentasHasCompanies)) {
            return false;
        }
        PygHasCuentasHasCompanies other = (PygHasCuentasHasCompanies) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Blue.PygHasCuentasHasCompanies[ id=" + id + " ]";
    }
    
}

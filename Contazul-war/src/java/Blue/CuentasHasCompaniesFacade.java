/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Blue;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author ivan
 */
@Stateless
public class CuentasHasCompaniesFacade extends AbstractFacade<CuentasHasCompanies> {
    @PersistenceContext(unitName = "Contazul-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CuentasHasCompaniesFacade() {
        super(CuentasHasCompanies.class);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Blue;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ivan
 */
@Entity
@Table(name = "cuentas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cuentas.findAll", query = "SELECT c FROM Cuentas c"),
    @NamedQuery(name = "Cuentas.findByIdCuentas", query = "SELECT c FROM Cuentas c WHERE c.idCuentas = :idCuentas"),
    @NamedQuery(name = "Cuentas.findByActivo", query = "SELECT c FROM Cuentas c WHERE c.activo = :activo"),
    @NamedQuery(name = "Cuentas.findByPasivo", query = "SELECT c FROM Cuentas c WHERE c.pasivo = :pasivo"),
    @NamedQuery(name = "Cuentas.findByTipo", query = "SELECT c FROM Cuentas c WHERE c.tipo = :tipo"),
    @NamedQuery(name = "Cuentas.findByNombre", query = "SELECT c FROM Cuentas c WHERE c.nombre = :nombre")})
public class Cuentas implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idCuentas")
    private Integer idCuentas;
    @Column(name = "Activo")
    private Integer activo;
    @Column(name = "pasivo")
    private Integer pasivo;
    @Column(name = "tipo")
    private Integer tipo;
    @Size(max = 45)
    @Column(name = "Nombre")
    private String nombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cuentasidCuentas1")
    private Collection<CuentasHasCompanies> cuentasHasCompaniesCollection;

    public Cuentas() {
    }

    public Cuentas(Integer idCuentas) {
        this.idCuentas = idCuentas;
    }

    public Integer getIdCuentas() {
        return idCuentas;
    }

    public void setIdCuentas(Integer idCuentas) {
        this.idCuentas = idCuentas;
    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }

    public Integer getPasivo() {
        return pasivo;
    }

    public void setPasivo(Integer pasivo) {
        this.pasivo = pasivo;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public Collection<CuentasHasCompanies> getCuentasHasCompaniesCollection() {
        return cuentasHasCompaniesCollection;
    }

    public void setCuentasHasCompaniesCollection(Collection<CuentasHasCompanies> cuentasHasCompaniesCollection) {
        this.cuentasHasCompaniesCollection = cuentasHasCompaniesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCuentas != null ? idCuentas.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cuentas)) {
            return false;
        }
        Cuentas other = (Cuentas) object;
        if ((this.idCuentas == null && other.idCuentas != null) || (this.idCuentas != null && !this.idCuentas.equals(other.idCuentas))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Blue.Cuentas[ idCuentas=" + idCuentas + " ]";
    }
    
}

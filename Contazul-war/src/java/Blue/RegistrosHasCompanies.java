/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Blue;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ivan
 */
@Entity
@Table(name = "registros_has_companies")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RegistrosHasCompanies.findAll", query = "SELECT r FROM RegistrosHasCompanies r"),
    @NamedQuery(name = "RegistrosHasCompanies.findById", query = "SELECT r FROM RegistrosHasCompanies r WHERE r.id = :id")})
public class RegistrosHasCompanies implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "Cuentas_has_Companies_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private CuentasHasCompanies cuentashasCompaniesid;
    @JoinColumn(name = "Registros_idRegistros", referencedColumnName = "idRegistros")
    @ManyToOne(optional = false)
    private Registros registrosidRegistros;

    public RegistrosHasCompanies() {
    }

    public RegistrosHasCompanies(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CuentasHasCompanies getCuentashasCompaniesid() {
        return cuentashasCompaniesid;
    }

    public void setCuentashasCompaniesid(CuentasHasCompanies cuentashasCompaniesid) {
        this.cuentashasCompaniesid = cuentashasCompaniesid;
    }

    public Registros getRegistrosidRegistros() {
        return registrosidRegistros;
    }

    public void setRegistrosidRegistros(Registros registrosidRegistros) {
        this.registrosidRegistros = registrosidRegistros;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegistrosHasCompanies)) {
            return false;
        }
        RegistrosHasCompanies other = (RegistrosHasCompanies) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Blue.RegistrosHasCompanies[ id=" + id + " ]";
    }
    
}

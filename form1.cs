using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1(string x)
        {
            InitializeComponent(x);
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Error Message", "Error Title", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);


            string nombre=this.tex1.Text;
            string nit = this.tex2.Text;

            WebRequest request = WebRequest.Create("http://localhost:8080/Contazul-war/webresources/blue.companies");

            request.Credentials = CredentialCache.DefaultCredentials;

            request.Method = "POST";

            string postData = "<companies><idCompanies></idCompanies><nit>111444422222</nit><nombre>token</nombre></companies>";
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            request.ContentType = "application/xml";

            request.ContentLength = byteArray.Length;
            // Get the request stream.
            Stream dataStream = request.GetRequestStream();
            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);
            // Close the Stream object.
            dataStream.Close();
            // Get the response.
            WebResponse response = request.GetResponse();
            // Display the status.
            Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            // Get the stream containing content returned by the server.
            dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            // Display the content.
            Console.WriteLine(responseFromServer);
            // Clean up the streams.
            reader.Close();
            dataStream.Close();
            response.Close();


        
        }
       

        
    }
}
